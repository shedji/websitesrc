import { Component, OnInit } from '@angular/core';
import { ApiService } from "../api.service";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Team } from  '../team';
import { Ladder } from '../ladder';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  ladderInfo: Ladder[];
  teamInfo: Team[];

  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.getTeams();
    this.getLadder();

  }

  public getLadder(){

    this.api.getLadder("q=standings;year=2019;round=20").subscribe(resp => this.ladderInfo = resp);    

  }

  getTeams(){

    this.api.getTeams().subscribe(resp => this.teamInfo = resp);    
    
  }

  getSName(team: string){
    let sName = "";
    this.teamInfo.forEach(element => {if(element.name == team){
      sName = element.abbrev;
      return;
    }})
    return sName;

  }
  getTeamLogo(team: number){
    team = team - 1;
    return this.teamInfo[team].logo;

  }

}
