import { Component, OnInit } from '@angular/core';
import { ApiService } from "../api.service";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Team } from '../team';
import { Ladder } from '../ladder';



@Component({
  selector: 'app-ladder',
  templateUrl: './ladder.component.html',
  styleUrls: ['./ladder.component.css']
})
export class LadderComponent implements OnInit {

  // Initialize variables and objects
  ladderInfo: Ladder[];
  teamInfo: Team[];
   
  constructor(private api: ApiService) { }
  
  ngOnInit(){
    
    // Initialize the data for the objects    
    this.getTeams();
    this.getLadder();
  
    
  }

  // A function to get the lada data
  public getLadder(){
    // Make a call to the api to get the current round ladder, hardcoded to round 20 for this assignment
    this.api.getLadder("q=standings;year=2019;round=20").subscribe(resp => this.ladderInfo = resp);

  }

  // A function to get the team information
  getTeams(){
    // Make a call to the api to get the team information
    this.api.getTeams().subscribe(resp => this.teamInfo = resp);

  }

  // A function to get the tream logo
  getTeamLogo(team: number){
    team = team - 1;
    return this.teamInfo[team].logo;

  }

}
