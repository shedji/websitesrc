import { Component, OnInit } from '@angular/core';
import { ApiService } from "../api.service";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Team } from '../team';
import { Ladder } from '../ladder';
import { Round } from '../round';
import {FormsModule} from '@angular/forms';


@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit {

  // Initialize variables and objects
  rounds: Round[];
  ladderInfo: Ladder[];
  teamInfo: Team[];
  selectedTeam: Ladder;
  hasBeenSelected: boolean = false;
  teamWonRounds: Round[];
  teamLostRounds: Round[];
   
  constructor(private api: ApiService) { }
  ngOnInit(){
    
    // Initialize data
    this.getTeams();
    this.getLadder();
    this.getRounds();
      
  }

  // A function to get the ladder information
  public getLadder(){
    // Make a call to the api to get the ladder information
    this.api.getLadder("q=standings;year=2019;round=20").subscribe(resp => {this.ladderInfo = resp; this.selectedTeam = resp[0]});    

  }

  // A function to get team information
  getTeams(){
    // Make a call to the api to get team information
    this.api.getTeams().subscribe(resp => this.teamInfo = resp);
    
    
  }

  // A function to get round information
  public getRounds(){
    // Make a call to the api to get round information
    this.api.getAllRoundInfo().subscribe(resp =>{ this.rounds = resp; console.log(this.rounds); });
 
  }

  // A function to get a team logo given name
  getTeamLogo(team: number){
    team = team - 1;
    return this.teamInfo[team].logo;

  }

  // A function to handle when a team is selected
  onSelect(team: Ladder){

    // set selected team
    this.selectedTeam = team;
    // Empty the won round array
    this.teamWonRounds = [];
    // Loop through the rounds and push any rounds that were won
    this.rounds.forEach(element => {if(((element.hteam == this.selectedTeam.name &&
        element.hscore > element.ascore) ||
      (element.ateam == this.selectedTeam.name && element.ascore > element.hscore))){
          this.teamWonRounds.push(element);
        }});
    // empty the lost rounds array
    this.teamLostRounds = [];
    // Loop through the rounds a push any rounds that were lost
    this.rounds.forEach(element => {if(((element.hteam == this.selectedTeam.name &&
        element.hscore < element.ascore) ||
      (element.ateam == this.selectedTeam.name && element.ascore < element.hscore))){
          this.teamLostRounds.push(element);
        }});

    this.hasBeenSelected = true;

  }

  // A function to unselect a team
 unselect(){
   this.hasBeenSelected = false;
 }

}
