import { Component, OnInit, Input } from '@angular/core';
import { Ladder } from '../ladder';
import { Round } from '../round';
import { Team } from '../team';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-resultsbyteamchild',
  templateUrl: './resultsbyteamchild.component.html',
  styleUrls: ['./resultsbyteamchild.component.css']
})
export class ResultsbyteamchildComponent implements OnInit {

  // Get inputs from parent component
  @Input() team: Ladder;
  @Input() logo: String;
  @Input() round: Round[];
  @Input() ladder: Ladder[];

  teamInfo: Team[];
  constructor(private api: ApiService) { }

  ngOnInit(): void {
    // Initialize data
    this.getTeams();
    this.round.reverse();
  }

  // A function to get the team logo by name
  getTeamLogo(team: string){
    let logoAdd = "";
    this.teamInfo.forEach(element => {if(element.name == team){
      logoAdd = element.logo;
      return;
    }})
    return logoAdd;

  }

  // A function to get team information
  getTeams(){

    this.api.getTeams().subscribe(resp => this.teamInfo = resp);
    
  }

}
