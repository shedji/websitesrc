import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsbyteamchildComponent } from './resultsbyteamchild.component';

describe('ResultsbyteamchildComponent', () => {
  let component: ResultsbyteamchildComponent;
  let fixture: ComponentFixture<ResultsbyteamchildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultsbyteamchildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsbyteamchildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
