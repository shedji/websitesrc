import { Component, OnInit} from '@angular/core';
import { ApiService } from "../api.service";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Team } from '../team';
import { Ladder } from '../ladder';
import { Round } from '../round';
import { Headtohead } from '../headtohead';
import {FormsModule} from '@angular/forms';

@Component({
  selector: 'app-headtohead',
  templateUrl: './headtohead.component.html',
  styleUrls: ['./headtohead.component.css']
})
export class HeadtoheadComponent implements OnInit {
  
  // Declare variables and objects
  rounds: Round[];
  teamInfo: Team[];
  selectedTeam1: Team;
  selectedTeam2: Team;
  headToHeadGames: Headtohead[];
  has1BeenSelected: boolean = false;
  has2BeenSelected: boolean = false;

  constructor(private api: ApiService) { }

  ngOnInit(){
    
    // Initialize the teams and rounds objects
    this.getTeams();
    this.getRounds();
 
  }

  // A function to get all of the round information
  public getRounds(){

    // Make a call to the api to get the round information
    this.api.getAllRoundInfo().subscribe(resp =>{ this.rounds = resp;});

  }

  // A function to get all of the team information
  getTeams(){

    // Make a call to the api to get the team information
    this.api.getTeams().subscribe(resp => {this.teamInfo = resp; this.selectedTeam1 = this.teamInfo[0];
      this.selectedTeam2 = this.teamInfo[1]});
    
    
  }

  
  // Function to handle when the first team is selected from the menu
  onSelectFteam(team: Team){

    // Assign the team to the selected team 1 object
    this.selectedTeam1 = team;
    // Set team 1 as selected      
    this.has1BeenSelected = true;
    // If both teams have been selected  
    if(this.has1BeenSelected == true && this.has2BeenSelected == true){
      // Call the function to get the games for the selected teams
      this.getSelectedGames();    
     }
     
  }

  // Function to handle when the second team is selected from the menu
  onSelectSteam(team: Team){
    // Assign the team to the selected team 2 object
    this.selectedTeam2 = team;
    // Set team 2 as selected  
    this.has2BeenSelected = true;
    // If both teams have been selected 
    if(this.has1BeenSelected == true && this.has2BeenSelected == true){
      // Call the function to get the games for the selected teams
      this.getSelectedGames();
    }    

  }

  // A function to loop through the rounds array and push game information to the head to head object array
  getSelectedGames(){
    // Clear the head to head array
    this.headToHeadGames = [];
    // for each round in the round array
    this.rounds.forEach(element => {
      // if both teams match the home and away teams
      if(element.hteam == this.selectedTeam1.name && element.ateam == this.selectedTeam2.name){
        // push the game information to the head to head object array
        this.headToHeadGames.push(new Headtohead(element.hteam, element.ateam, element.hgoals, element.agoals, element.hbehinds, element.abehinds,
          element.hscore, element.ascore, element.venue, element.winner, element.round, element.complete));

      }
      // if both teams match the home and away teams
      else if(element.hteam == this.selectedTeam2.name && element.ateam == this.selectedTeam1.name){
        // push the game information to the head to head object array
        this.headToHeadGames.push(new Headtohead(element.ateam, element.hteam, element.agoals, element.hgoals, element.abehinds, element.hbehinds,
          element.ascore, element.hscore, element.venue, element.winner, element.round, element.complete));

      }
    });

  }

  // A function to unselect first team
  unselectFteam(){

    this.has1BeenSelected == false

  }

  // A function to unselect second team
  unselectSteam(){

    this.has2BeenSelected == false

  }

}
