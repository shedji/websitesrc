import { Component, OnInit } from '@angular/core';
import { ApiService } from "../api.service";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Round } from '../round';
import { Team } from  '../team';
import { FooterComponent } from '../footer/footer.component';
import {FormsModule} from '@angular/forms';



@Component({
  selector: 'app-rounds',
  templateUrl: './rounds.component.html',
  styleUrls: ['./rounds.component.css']
})
export class RoundsComponent implements OnInit {

  // Initialize variables and objects
  // Round number hardcoded for this assignment
  currentRound: number = 20;
  roundNumbers: number = 5;
  remainingRounds: number[];
  roundsByTeam: Round[] = [];
  byTeam: boolean = false;
  rounds: Array<Round[]> = [];
  teamInfo: Team[];
  selectedTeam: string = "";

  constructor(private api: ApiService) { }

  ngOnInit(): void {

    // Initialize data
    this.getTeams();    
    this.initRounds(this.roundNumbers);
    this.rounds.sort(this.compareFunc);
  
  }

  // A function to populate a list of remaining round numbers and get round information
  initRounds(num: number){
    let y = 1;
    this.remainingRounds = [];
    this.roundsByTeam = [];
    this.roundNumbers = Number(num);
    for(let x = this.currentRound; x <= 27; x++){
      this.remainingRounds.push(y);
      y++;
    }
    
    this.rounds = [];

    // If displaying by team
    if(this.byTeam){
      // Get the required round information for that team
      for(let i = this.currentRound; i < 28 + this.roundNumbers; i++){       
        this.getRoundByTeam(i);       
      }
    }
    // If displaying by number of rounds
    else{
      // Get the rounds that were selected
      for(let i = this.currentRound; i < this.currentRound + this.roundNumbers; i++){           
          this.getRound(i);
        }

    }    

  }
  
  // A function to Get a round by round number
  getRound(num: number){
    // Make a call to the api to get a round by round number
    this.api.getRoundInfo(num).subscribe(resp => {this.rounds.push(resp); this.rounds.sort(this.compareFunc);});
    
  }
  
  //  A function to get round information by team name
  getRoundByTeam(num: number){
    // Get round information by round number
    this.api.getRoundInfo(num).subscribe(resp => {
      resp.forEach(element => {
        // If the team name appears in the round information push it to the rounds by team array
        if(element.ateam == this.selectedTeam || element.hteam == this.selectedTeam){
          this.roundsByTeam.push(element); this.roundsByTeam.sort(this.compareFuncTeam);
          console.log("OK");
        }
      })      
    });
    
  }

  // a compare function to sort rounds by round number when an array of arrays is used
  compareFunc(a,b){

      const gameAround = a[0].round;
      const gameBround = b[0].round;

      let compare = 0;

      if(gameAround < gameBround){

        compare = -1;

      }else if (gameAround > gameBround){

        compare = 1;

      }

      return compare;

  }
  // A compare function to sort rounds by round number when a normal array is used
  compareFuncTeam(a,b){

    const gameAround = a.round;
    const gameBround = b.round;

    let compare = 0;

    if(gameAround < gameBround){

      compare = -1;

    }else if (gameAround > gameBround){

      compare = 1;

    }

    return compare;

  }

  // A function to get team information
  getTeams(){
  // Make a call to the api to get team information
    this.api.getTeams().subscribe(resp => this.teamInfo = resp);
    
    
  }

  // A function to get a team logo given team name
  getTeamLogo(team: string){
    let logoAdd = "";
    this.teamInfo.forEach(element => {if(element.name == team){
      logoAdd = element.logo;
      return;
    }})
    return logoAdd;
  }

  // A function to set the filtering by team
  setByTeam(){
    this.byTeam = true;
    if(this.selectedTeam != ""){
      this.getByTeam(this.selectedTeam);
    }
  }

  // A function to set filtering to none
  setByAll(){
    this.byTeam = false;
    this.initRounds(this.roundNumbers);
  }

  // A function to get the rounds by team
  getByTeam(team: string){
    this.selectedTeam = team;
    this.initRounds(this.roundNumbers);
  }

  
}


