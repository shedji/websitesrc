import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LadderComponent } from './ladder/ladder.component';
import { TeamsComponent } from './teams/teams.component';
import { RoundsComponent } from './rounds/rounds.component';
import { HeadtoheadComponent } from './headtohead/headtohead.component';
import { ResultsComponent } from './results/results.component';
import { TipsComponent } from './tips/tips.component';
import { ResultsbyteamComponent } from './resultsbyteam/resultsbyteam.component';


const routes: Routes = [
  {path: '', component: LadderComponent},
  {path: 'ladder', component: LadderComponent},
  {path: 'teams', component: TeamsComponent},
  {path: 'rounds', component: RoundsComponent},
  {path: 'headtohead', component: HeadtoheadComponent},
  {path: 'results', component: ResultsComponent},
  {path: 'resultsbyteam', component: ResultsbyteamComponent},
  {path: 'tips', component: TipsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
