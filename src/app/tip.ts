export class Tip {
    constructor(public round: number, public hteam: string, public ateam: string, public venue: string, public source: string, public tip: string, public confidence: number){}
}
