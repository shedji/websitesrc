import { Component, OnInit, Input } from '@angular/core';
import { Ladder } from '../ladder';
import { Round } from '../round';
@Component({
  selector: 'app-teamchild',
  templateUrl: './teamchild.component.html',
  styleUrls: ['./teamchild.component.css']
})
export class TeamchildComponent implements OnInit {
  // Get the inputs from the parent component
  @Input() team: Ladder;
  @Input() logo: String;
  @Input() venuesWon: Round[];
  @Input() venuesLost: Round[]
  constructor() { }

  ngOnInit(): void {
  }

  // initialize the hamburber menu
  hamburger() {
    var x = document.getElementById("myLinks");
    if (x.style.display === "block") {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
}

}
