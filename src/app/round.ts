export class Round {
    constructor(public round: number, public hteam: string, public ateam: string, public date: string, public venue: string, public hscore: number,
        public hgoals: number, public hbehinds: number, public ascore: number, public agoals: number, public abehinds: number, public winner: string, public complete: number){}
}
