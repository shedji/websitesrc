import { Component, OnInit } from '@angular/core';
import { ApiService } from "../api.service";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Tip } from '../tip';
import { Team } from  '../team';
import {FormsModule} from '@angular/forms';

@Component({
  selector: 'app-tips',
  templateUrl: './tips.component.html',
  styleUrls: ['./tips.component.css']
})
export class TipsComponent implements OnInit {
  
  // Initialize variables and objects
  // Round number is hard coded for this assignment
  currentRound: number = 20;
  tips: Tip[] = [];
  teamInfo: Team[];
  selectedTeam: Team;
  selected: boolean;
  constructor(private api: ApiService) { }

  ngOnInit(): void {
    // Initialize data
    this.getTeams();
  }

  // A function to handle when a team is selected
  onSelect(team: Team){
    // empty the tips array
    this.tips= [];
    // Set the selected team
    this.selected = true;
    this.selectedTeam = team;
    // Get the tip information by team
    this.getTipsByTeam(this.currentRound);

  }

  // A function to unselect a team
  unselect(){
    this.selected = false;
    this.selectedTeam = null;
  }

  // A function to get the tips for a team given the round number
  getTipsByTeam(num: number){
    // Make a call to the api to get the tips and push any tips that match the selected team
    this.api.getTipsInfo(num).subscribe(resp => {
      resp.forEach(element => {
        if(element.ateam == this.selectedTeam.name || element.hteam == this.selectedTeam.name){
          this.tips.push(element);
        }
      })      
    });
    
  }
  // A function to get team information
  getTeams(){
    // Make a call to the api to get team information
    this.api.getTeams().subscribe(resp => this.teamInfo = resp);
    
    
  }

  // A function to get a team logo given team name
  getTeamLogo(team: string){
    let logoAdd = "";
    this.teamInfo.forEach(element => {if(element.name == team){
      logoAdd = element.logo;
      return;
    }})
    return logoAdd;

  }

}
