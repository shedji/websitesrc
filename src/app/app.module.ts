import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { LadderComponent } from './ladder/ladder.component';
import { TeamsComponent } from './teams/teams.component';
import { RoundsComponent } from './rounds/rounds.component';
import { SortbyPipe } from './sortby.pipe';
import { TeamchildComponent } from './teamchild/teamchild.component';
import { HeadtoheadComponent } from './headtohead/headtohead.component';
import { HeadtoheadchildComponent } from './headtoheadchild/headtoheadchild.component';
import { ResultsComponent } from './results/results.component';
import { TipsComponent } from './tips/tips.component';
import { TipschildComponent } from './tipschild/tipschild.component';
import { ResultsbyteamComponent } from './resultsbyteam/resultsbyteam.component';
import { ResultsbyteamchildComponent } from './resultsbyteamchild/resultsbyteamchild.component';

@NgModule({
  declarations: [
    AppComponent,
    SidenavComponent,
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    LadderComponent,
    TeamsComponent,
    RoundsComponent,
    SortbyPipe,
    TeamchildComponent,
    HeadtoheadComponent,
    HeadtoheadchildComponent,
    ResultsComponent,
    TipsComponent,
    TipschildComponent,
    ResultsbyteamComponent,
    ResultsbyteamchildComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule ,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
