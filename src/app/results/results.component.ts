import { Component, OnInit } from '@angular/core';
import { Round } from '../round';
import { Team } from  '../team';
import { ApiService } from "../api.service";
import {FormsModule} from '@angular/forms';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  // Initialize variables and objects
  // Hardcoded round number for this assignment
  currentRound: number = 20;
  selectedRound: number = 19;
  round: Round[] = [];
  teamInfo: Team[];
  rounds: number[] = [];

  constructor(private api: ApiService) { }

  ngOnInit(): void {

    // get the team information
    this.getTeams();
    // If the current round is after round 1
    if(this.currentRound > 1){
      // Get the round results for the previous round
      this.getRound(this.currentRound - 1);
    }

    // populate an array with the available round numbers
    for(let i = 1; i < this.currentRound; i++){

      this.rounds.push(i);

    }
  
  }
  
  // A function to get round information given a round number
  getRound(num: number){
    // Make a call to the api to get round information
    this.api.getRoundInfo(num).subscribe(resp => this.round = resp);
    
  }

  // A function to get team information
  getTeams(){
    // Make a call to the api to get team information
    this.api.getTeams().subscribe(resp => this.teamInfo = resp);
    
    
  }

  // A function to get the team logo given a team name
  getTeamLogo(team: string){
    let logoAdd = "";
    this.teamInfo.forEach(element => {if(element.name == team){
      logoAdd = element.logo;
      return;
    }})
    return logoAdd;

  }

}
