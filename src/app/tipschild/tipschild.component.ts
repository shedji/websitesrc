import { Component, OnInit, Input } from '@angular/core';
import { Tip } from '../tip';
import { Team } from  '../team';

@Component({
  selector: 'app-tipschild',
  templateUrl: './tipschild.component.html',
  styleUrls: ['./tipschild.component.css']
})
export class TipschildComponent implements OnInit {
  // Get the input from the parent component
  @Input() teamInfo: Team[];
  @Input() tips: Tip[];
  constructor() { }

  ngOnInit(): void {
  }

  // get a team logo given team name
  getTeamLogo(team: string){
    let logoAdd = "";
    this.teamInfo.forEach(element => {if(element.name == team){
      logoAdd = element.logo;
      return;
    }})
    return logoAdd;

  }

}
