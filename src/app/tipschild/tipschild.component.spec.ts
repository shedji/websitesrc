import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipschildComponent } from './tipschild.component';

describe('TipschildComponent', () => {
  let component: TipschildComponent;
  let fixture: ComponentFixture<TipschildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipschildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipschildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
