import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadtoheadchildComponent } from './headtoheadchild.component';

describe('HeadtoheadchildComponent', () => {
  let component: HeadtoheadchildComponent;
  let fixture: ComponentFixture<HeadtoheadchildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadtoheadchildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadtoheadchildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
