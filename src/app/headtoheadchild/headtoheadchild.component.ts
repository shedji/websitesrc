import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { Headtohead } from '../headtohead';
import { Team } from '../team';
import { ElementSchemaRegistry } from '@angular/compiler';

@Component({
  selector: 'app-headtoheadchild',
  templateUrl: './headtoheadchild.component.html',
  styleUrls: ['./headtoheadchild.component.css']
})
export class HeadtoheadchildComponent implements OnInit {
  // Get inputs from the parent component
  @Input() rounds: Headtohead[];
  @Input() teamInfo: Team[];

  // Initialize variables and objects
  team1Wins: number = 0;
  team2Wins: number = 0;
  team1Score: number = 0;
  team2Score: number = 0;
  team1Goals: number = 0;
  team2Goals: number = 0;
  team1Behinds: number = 0;
  team2Behinds: number = 0;
  team1Name: string;
  team2Name: string;
  draws: number;

  constructor() { }

  ngOnInit(): void {
    // Call the function to tally up the total stats between the teams
    this.getTotalStats();

  }

  // A function to re adjust the stats when a change is detected
  ngOnChanges(changes: SimpleChanges){
    // Call the function to tally up the total stats between the teams 
    this.getTotalStats();
  }

  // A function to initialize the scores to zero when a re-calculation is required
  initScores(){

    this.team1Wins = 0;
    this.team2Wins = 0;
    this.team1Score = 0;
    this.team2Score = 0;
    this.team1Behinds = 0;
    this.team1Goals = 0;
    this.team2Behinds = 0;
    this.team2Goals = 0;
    this.draws = 0;

  }

  // A function to get the logo of a team
  getTeamLogo(team: string){
    let logoAdd = "";
    this.teamInfo.forEach(element => {if(element.name == team){
      logoAdd = element.logo;
      return;
    }})
    return logoAdd;
  }

  // A function to calculate the total stats between the two teams
  getTotalStats(){
    // Initialize the scores to 0
    this.initScores();
    // Set the team names
    this.team1Name = this.rounds[0].team1Name;
    this.team2Name = this.rounds[0].team2Name;
    // Loop through the rounds array
      this.rounds.forEach(element => {
        // Set the values for both team stats
        this.team1Score += element.team1Score;
        this.team2Score += element.team2Score;
        this.team1Behinds += element.team1Behinds;
        this.team2Behinds += element.team2Behinds;
        this.team1Goals += element.team1Goals;
        this.team2Goals += element.team2Goals;
        // Update the win tally for the teams
        if(element.winner == this.team1Name){
          this.team1Wins ++;
        }else if(element.winner == this.team2Name){
          this.team2Wins ++;
        }else{
          this.draws ++;
        }
      });

  }

}
