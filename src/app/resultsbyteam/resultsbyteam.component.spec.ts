import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsbyteamComponent } from './resultsbyteam.component';

describe('ResultsbyteamComponent', () => {
  let component: ResultsbyteamComponent;
  let fixture: ComponentFixture<ResultsbyteamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultsbyteamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsbyteamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
