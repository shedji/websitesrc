import { Component, OnInit } from '@angular/core';
import { Round } from '../round';
import { Team } from  '../team';
import { ApiService } from "../api.service";
import { Ladder } from '../ladder';
import {FormsModule} from '@angular/forms';

@Component({
  selector: 'app-resultsbyteam',
  templateUrl: './resultsbyteam.component.html',
  styleUrls: ['./resultsbyteam.component.css']
})
export class ResultsbyteamComponent implements OnInit {

  // Initialize variables and objects
  rounds: Round[];
  ladderInfo: Ladder[];
  teamInfo: Team[];
  selectedTeam: Ladder;
  selectedTeam1: Ladder;
  hasBeenSelected: boolean = false;
   
  constructor(private api: ApiService) { }
  ngOnInit(){
  
    // Initialize data
    this.getTeams();
    this.getLadder();
    this.getRounds();
  
  }

  // A function to get the ladder information
  public getLadder(){
    // Make a call to the api to get ladder information
    this.api.getLadder("q=standings;year=2019;round=20").subscribe(resp => {this.ladderInfo = resp; this.selectedTeam = resp[0]});    
  }

  // A function to get team information
  getTeams(){
    // Make a call to the api to get team information
    this.api.getTeams().subscribe(resp => this.teamInfo = resp);
  }

  // A function to get round information
  public getRounds(){
    // Make a call to the api to get round information
    this.api.getAllRoundInfo().subscribe(resp =>{ this.rounds = resp; console.log(this.rounds); });
  }

  // A function to get the team logo
  getTeamLogo(team: number){
    team = team - 1;
    return this.teamInfo[team].logo;
  }

  // A function to select a team to view the results for
  onSelect(team: Ladder){
    this.selectedTeam = team;
    this.hasBeenSelected = true;
  }

  // A function to unselect a team
 unselect(){
   this.hasBeenSelected = false;
 }

}
