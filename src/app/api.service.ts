import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map } from "rxjs/operators";
import { Team } from './team';
import { Ladder } from './ladder';
import { Round } from './round';
import { Tip } from './tip';

const httpOptions = {
  headers: new HttpHeaders({})
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient) { }

    private url = "https://api.squiggle.com.au/?";

    ngOnInit() {

      
      
    }

    // A function to make a call to the squiggle api to get the ladder information, returns a ladder object array
    getLadder(params: String): Observable<Ladder[]>{
      return this.http.get<any>(this.url + params).pipe(map((data: any)=> data.standings.map((item: any)=>new Ladder(item.rank, item.id, item.name, item.played, item.pts, item.percentage, item.wins, item.losses, item.draws, item.for,  item.against, item.goals_for, item.behinds_for))));
    }

    // A function to make a call to the squiggle api to get the roiund information given a round number, returns a Round object array
    getRoundInfo(rnd: number): Observable<Round[]>{

      return this.http.get<any>(this.url + "q=games;year=2019;round=" + rnd).pipe(map((data: any)=> data.games.map((item: any)=>new Round(item.round, item.hteam, item.ateam, item.date, item.venue, item.hscore, item.hgoals, item.hbehinds, item.ascore, item.agoals, item.abehinds, item.winner, item.complete))));

    }

    // A function to make a call to the squiggle api to get tips information given a round number, returns a Tips object array
    getTipsInfo(rnd: number): Observable<Tip[]>{

      return this.http.get<any>(this.url + "q=tips;year=2019;round=" + rnd).pipe(map((data: any)=> data.tips.map((item: any)=>new Tip(item.round, item.hteam, item.ateam, item.venue, item.source, item.tip, item.confidence))));

    }

    // A function to make a call to the squiggle api to get all round information for 2019, returns a round object array
    getAllRoundInfo(): Observable<Round[]>{

      return this.http.get<any>(this.url + "q=games;year=2019").pipe(map((data: any)=> data.games.map((item: any)=>new Round(item.round, item.hteam, item.ateam, item.date, item.venue, item.hscore, item.hgoals, item.hbehinds, item.ascore, item.agoals, item.abehinds, item.winner, item.complete))));

    }

    // A function to make a call to the squiggle api to get team information, returns a team object array
    getTeams(): Observable<Team[]>{

      return this.http.get(this.url + "q=teams").pipe(map((data: any)=> data.teams.map((item: any)=>new Team(item.name, item.id, item.abbrev, item.logo))));

    }
 

}
