export class Headtohead {
    constructor(public team1Name: string, public team2Name: string, public team1Goals: number, public team2Goals:number, public team1Behinds: number, public team2Behinds: number,
        public team1Score: number, public team2Score: number, public venue: string, public winner: string, public round: number, public complete: number){}
}
