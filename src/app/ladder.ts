export class Ladder {
    constructor(public rank: number, public id: number, public name: string, public played: number, public pts: number, public percentage: number, public wins: number, public losses: number, public draws: number, public ptsfor: number, public ptsagainst: number, public goalsfor: number, public behindsfor: number){}
}
